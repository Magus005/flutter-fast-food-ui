import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'models/wings.dart';


class DetailsWing extends StatelessWidget {
  DetailsWing(this.wingObject);

  final Wing wingObject;

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(wingObject.background);
    return Scaffold(
      body: Center(
        child: ListView(
          children: <Widget>[
            BackgroundArc(wingObject.background),
            ForegroundContent(wingObject: wingObject),
          ],
        ),
      ),
    );
  }

}


class BackgroundArc extends StatelessWidget {
  const BackgroundArc(this.background);

  final Color background;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CustomPaint(
        painter: BackgroundPainter(background),
      ),
    );
  }
}

class BackgroundPainter extends CustomPainter {
  BackgroundPainter(this.color);

  final Color color;

  Path path = Path();

  @override
  void paint(Canvas canvas, Size size) {
    Paint painter = Paint()..color = color;
    path.moveTo(250, 0);
    path.quadraticBezierTo(150, 125, 240, 270);
    path.quadraticBezierTo(300, 345, 450, 350);
    path.lineTo(500, 0);
    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
  
}


class ForegroundContent extends StatelessWidget {
  const ForegroundContent({@required this.wingObject});

  final Wing wingObject;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(top: 70, left: 50),
            child: Container(
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back, size: 30),
              ),
            ),
          ),
        ),

        WingImage(wingObject.image),
        
        SizedBox(height: 30),
        Padding(
          padding: EdgeInsets.only(left: 105, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TitleText(wingObject.name),
              SizedBox(height: 20),
              StarRating(wingObject.starRating),
              SizedBox(height: 20),
              Description(wingObject.desc),
              SizedBox(height: 20),
              Price(wingObject.price),
              SizedBox(height: 20),
              BottomButton(),
              SizedBox(height: 20),
            ],
          ),
        )

      ],
    );
  }
}


class TitleText extends StatelessWidget {
  const TitleText(this.wingName);

  final String wingName;
  final double _fontSize = 40;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: wingName, style: TextStyle(color: Colors.black, fontSize: _fontSize, fontWeight: FontWeight.w500, fontFamily: "slabo")),
          TextSpan(text: " Wing", style: TextStyle(color: Colors.black, fontSize: _fontSize, fontWeight: FontWeight.w600, fontFamily: "slabo")),
        ]
      ),
    );
  }
  
}


class WingImage extends StatelessWidget {
  const WingImage(this.imageUri);

  final String imageUri;


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 300,
      child: Image.asset(imageUri),
    );
  }
  
}


class StarRating extends StatelessWidget {
  final double rating;

  const StarRating(this.rating);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(rating.toString(), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20)),
        SizedBox(width: 5),
        Icon(Icons.star, color: Colors.yellow[600], size: 25)
      ],
    );
  }
}

class Description extends StatelessWidget {
  const Description(this.desc);

  final String desc;

  @override
  Widget build(BuildContext context) {
    return Text(
      desc,
      softWrap: true,
      style: TextStyle(color: Colors.black87, letterSpacing: 1.3, fontSize: 17, textBaseline: TextBaseline.alphabetic),
    );
  }
  
}

class Price extends StatelessWidget {
  const Price(this.price);

  final double price;


  @override
  Widget build(BuildContext context) {
    return Text("\$$price", style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.w700));
  }
  
}

class BottomButton extends StatefulWidget {
  @override
  BottomButtonState createState() => BottomButtonState();
}

class BottomButtonState extends State<BottomButton> {
  bool isFavori = false;
  bool isCart = false;

  @override
  void initState() { 
    super.initState();
    isFavori = false;
    isCart = false;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.black,
                width: 5
              )
            )
          ),
          child: FlatButton(
            onPressed: () { 
              setState(() {
                isCart = !isCart;
              });
            },
            child: Text(isCart ? "Remove from cart" : "Add to cart", style: TextStyle(fontWeight: FontWeight.w800, fontSize: 18)),

          ),
        ),

        FloatingActionButton(
          onPressed: () {
            setState(() {
              isFavori = !isFavori;
            });
          },
          child: Icon(
            isFavori ? Icons.favorite : Icons.favorite_border,
            color: isFavori ? Colors.red : Colors.black,
          ),
          backgroundColor: Colors.white,
          elevation: 3,
        )
      ],
    );
  }
  
}