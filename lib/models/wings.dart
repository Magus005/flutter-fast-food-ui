import 'package:flutter/material.dart';

WingsList wingList = WingsList(wings: [
  Wing(
    image: "assets/images/howie_wings.png",
    starRating: 4.5,
    name: "Howie",
    desc: "Mildly seasoned chicken wings with your choice of dipping sauce or coated in Asian, BBQ, Buffalo or Sriracha sauce.",
    background: Color(0xfff2ca80),
    foreground: Colors.black,
    price: 6000,
  ),
  Wing(
    image: "assets/images/asian_howie_wings.png",
    starRating: 4.5,
    name: "Asian Howie",
    desc: "Coated in our tangy Asian sauce.",
    background: Color(0xffd82a12),
    foreground: Colors.white,
    price: 6000,
  ),
  Wing(
    image: "assets/images/original_boneless_howie_wings.png",
    starRating: 4.5,
    name: "Original Boneless Howie",
    desc: "Tender, lightly breaded 100% chicken breast.",
    background: Color(0xff5d2512),
    foreground: Colors.white,
    price: 6000,
  ),
]);

class WingsList {
  List<Wing> wings;
  WingsList({@required this.wings});
}

class Wing {
  String image;
  Color background;
  Color foreground;
  String name;
  double starRating;
  String desc;
  double price;

  Wing(
    {
      @required this.image,
      @required this.background,
      @required this.foreground,
      @required this.name,
      @required this.starRating,
      @required this.desc,
      @required this.price
    }
  );
}