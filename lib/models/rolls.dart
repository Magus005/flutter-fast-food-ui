import 'package:flutter/material.dart';

RollsList rollList = RollsList(rolls: [
  Roll(
    image: "assets/images/pepperoni_cheese.png",
    starRating: 4.5,
    name: "Pepperoni & Cheese",
    desc: "Pepperoni & melted mozzarella cheese rolled in freshly made pizza dough, baked to perfection with dipping sauce available upon request.",
    background: Color(0xfff2ca80),
    foreground: Colors.black,
    price: 6000,
  ),
  Roll(
    image: "assets/images/chicken_cheese.png",
    starRating: 4.5,
    name: "Chicken & Cheese",
    desc: "Grilled chicken breast & melted cheddar cheese rolled in freshly made pizza dough, baked to perfection with dipping sauce available upon request.",
    background: Color(0xffd82a12),
    foreground: Colors.white,
    price: 6000,
  ),
  Roll(
    image: "assets/images/steak_cheese.png",
    starRating: 4.5,
    name: "Steak & Cheese",
    desc: "Marinated steak & melted cheddar cheese rolled in freshly made pizza dough, baked to perfection with dipping sauce available upon request.",
    background: Color(0xff5d2512),
    foreground: Colors.white,
    price: 6000,
  ),
]);

class RollsList {
  List<Roll> rolls;
  RollsList({@required this.rolls});
}

class Roll {
  String image;
  Color background;
  Color foreground;
  String name;
  double starRating;
  String desc;
  double price;

  Roll(
    {
      @required this.image,
      @required this.background,
      @required this.foreground,
      @required this.name,
      @required this.starRating,
      @required this.desc,
      @required this.price
    }
  );
}