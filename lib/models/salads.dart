import 'package:flutter/material.dart';

SaladsList saladList = SaladsList(salads: [
  Salad(
    image: "assets/images/antipasto_salad.png",
    starRating: 4.5,
    name: "Antipasto",
    desc: "Ham, salami, mozzarella cheese, tomatoes, red onions, black olives & pepperoncini peppers over a fresh romaine mix with Italian dressing on the side.",
    background: Color(0xfff2ca80),
    foreground: Colors.black,
    price: 6000,
  ),
  Salad(
    image: "assets/images/garden.png",
    starRating: 4.5,
    name: "Garden",
    desc: "Tomatoes, red onions, cucumbers, green peppers, black olives & a side of croutons over a fresh romaine mix with your choice of dressing on the side.",
    background: Color(0xffd82a12),
    foreground: Colors.white,
    price: 6000,
  ),
  Salad(
    image: "assets/images/greek.png",
    starRating: 4.5,
    name: "Greek",
    desc: "Feta cheese, cucumbers, tomatoes, red onions, black olives & pepperoncini peppers over a fresh romaine mix with Greek dressing on the side.",
    background: Color(0xff5d2512),
    foreground: Colors.white,
    price: 6000,
  ),
]);

class SaladsList {
  List<Salad> salads;
  SaladsList({@required this.salads});
}

class Salad {
  String image;
  Color background;
  Color foreground;
  String name;
  double starRating;
  String desc;
  double price;

  Salad(
    {
      @required this.image,
      @required this.background,
      @required this.foreground,
      @required this.name,
      @required this.starRating,
      @required this.desc,
      @required this.price
    }
  );
}