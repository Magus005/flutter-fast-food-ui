import 'package:flutter/material.dart';

PizzaList pizzaList = PizzaList(pizzas: [
  Pizza(
    image: "assets/images/build_your_own_pizza.png",
    starRating: 4.5,
    name: "Build Your Own",
    desc: "Pizza dough made fresh daily is where it at all starts. Topped with 100% real mozzarella cheese, add any of our quality toppings to create your favorite combination. Choose from our original round, deep dish, thin crust* or gluten-free*. Don’t forget your free Flavored Crust®! *Not made in-store.",
    background: Color(0xfff2ca80),
    foreground: Colors.black,
    price: 6000,
  ),
  Pizza(
    image: "assets/images/meat_eaters_pizza.png",
    starRating: 4.5,
    name: "Meat Eaters",
    desc:
        "Pepperoni, ham, Italian sausage, ground beef & mozzarella cheese.",
    background: Color(0xffd82a12),
    foreground: Colors.white,
    price: 6000,
  ),
  Pizza(
    image: "assets/images/works_pizza.png",
    starRating: 4.5,
    name: "Works",
    desc:
        "Pepperoni, ham, Italian sausage, ground beef, mushrooms, red onions, green peppers, black olives & extra mozzarella cheese.",
    background: Color(0xff4fc420),
    foreground: Colors.black,
    price: 6000,
  ),
  Pizza(
    image: "assets/images/howie_maui_hawaiian_pizza.png",
    starRating: 4.5,
    name: "Howie Maui",
    desc:
        "Ham, bacon, pineapple & mozzarella cheese.",
    background: Color(0xff5d2512),
    foreground: Colors.white,
    price: 6000,
  ),
  Pizza(
    image: "assets/images/bbq_chicken_pizza.png",
    starRating: 4.5,
    name: "Bbq Chicken",
    desc:
        "Sweet BBQ sauce, grilled chicken breast, bacon, red onions & mozzarella cheese.",
    background: Color(0xffdddbd8),
    foreground: Colors.black,
    price: 6000,
  ),
  Pizza(
    image: "assets/images/asian_chicken_pizza.png",
    starRating: 4.5,
    name: "Asian Chicken",
    desc:
        "Tangy Asian sauce, grilled chicken breast, red onions, green peppers, sesame seeds & mozzarella cheese.",
    background: Color(0xffd54b1c),
    foreground: Colors.white,
    price: 6000,
  ),
]);

class PizzaList {
  List<Pizza> pizzas;
  PizzaList({@required this.pizzas});
}

class Pizza {
  String image;
  Color background;
  Color foreground;
  String name;
  double starRating;
  String desc;
  double price;

  Pizza(
    {
      @required this.image,
      @required this.background,
      @required this.foreground,
      @required this.name,
      @required this.starRating,
      @required this.desc,
      @required this.price
    }
  );
}