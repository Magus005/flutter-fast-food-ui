import 'package:flutter/material.dart';


class BottomBar extends StatelessWidget {
  final double _size = 60;
  final double _padding = 17;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.transparent,
      elevation: 0.0,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                final snackBar = SnackBar(content: Text('Profile !'));
                Scaffold.of(context).showSnackBar(snackBar);
              },
              child: Container(
                height: _size,
                width: _size,
                padding: EdgeInsets.all(_padding),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image.asset("assets/images/profile.jpg", fit: BoxFit.contain),
                ),
              ),
            ),

            GestureDetector(
              onTap: () {
                final snackBar = SnackBar(content: Text('Home !'));
                Scaffold.of(context).showSnackBar(snackBar);
              },
              child: Container(
                height: _size,
                width: _size,
                padding: EdgeInsets.all(_padding),
                child: Image.asset("assets/images/home_icon.png", fit: BoxFit.contain),
              ),
            ),

            GestureDetector(
              onTap: () { 
                final snackBar = SnackBar(content: Text('Search !'));
                Scaffold.of(context).showSnackBar(snackBar);
              },
              child: Container(
                height: _size,
                width: _size,
                padding: EdgeInsets.all(_padding),
                child: Image.asset("assets/images/search_icon.png", fit: BoxFit.contain),
              ),
            ),

            GestureDetector(
              onTap: () { 
                final snackBar = SnackBar(content: Text('Panier !'));
                Scaffold.of(context).showSnackBar(snackBar);
              },
              child: Container(
                height: _size,
                width: _size,
                padding: EdgeInsets.all(_padding),
                decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(50)),
                child: Image.asset("assets/images/bag_icon.png", fit: BoxFit.contain),
              ),
            )

          ],
        ),
      ),
    );
  }

}