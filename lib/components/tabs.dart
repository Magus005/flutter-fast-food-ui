import 'package:flutter/material.dart';
import './pizza/index.dart';
import './rolls/index.dart';
import './wings/index.dart';
import './salad/index.dart';

Widget tabs() {
  return Container(
    height: 580,
    width: double.infinity,
    child: DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(20),
            child: Container(
              color: Colors.transparent,
              child: SafeArea(
                child: Column(
                  children: <Widget>[
                    TabBar(
                      isScrollable: true,
                      labelPadding: EdgeInsets.only(top: 15),
                      indicatorColor: Colors.transparent,
                      labelColor: Colors.black,
                      labelStyle: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w800,
                        fontFamily: "slabo"
                      ),
                      unselectedLabelColor: Colors.black26,
                      unselectedLabelStyle: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w200,
                        fontFamily: "slabo"
                      ),
                      tabs: <Widget>[
                        Container(child: Text("Pizza")),
                        Container(padding: EdgeInsets.only(left: 60) ,child: Text("Rolls")),
                        Container(padding: EdgeInsets.only(left: 60) ,child: Text("Wings")),
                        Container(padding: EdgeInsets.only(left: 60) ,child: Text("Salads")),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),

        body: TabBarView(
          children: <Widget>[
            pizzaShowCase(),
            rollShowCase(),
            wingShowCase(),
            saladShowCase(),
          ],
        ),

      ),
    ),
  );
}