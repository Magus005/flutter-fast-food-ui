import 'package:flutter/material.dart';
import '../../models/pizza.dart';
import './item.dart';


Widget pizzaShowCase() {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 30),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: pizzaList.pizzas.length,
      itemBuilder: (BuildContext context, int i) {
        return ListOfPizzas(
          foreground: pizzaList.pizzas[i].foreground,
          background: pizzaList.pizzas[i].background,
          price: pizzaList.pizzas[i].price,
          name: pizzaList.pizzas[i].name,
          image: pizzaList.pizzas[i].image,
          pizzaObject: pizzaList.pizzas[i],
        );
      },
    ),
  );
}