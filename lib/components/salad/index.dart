import 'package:flutter/material.dart';
import '../../models/salads.dart';
import './item.dart';


Widget saladShowCase() {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 30),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: saladList.salads.length,
      itemBuilder: (BuildContext context, int i) {
        return ListOfSalads(
          foreground: saladList.salads[i].foreground,
          background: saladList.salads[i].background,
          price: saladList.salads[i].price,
          name: saladList.salads[i].name,
          image: saladList.salads[i].image,
          saladObject: saladList.salads[i],
        );
      },
    ),
  );
}