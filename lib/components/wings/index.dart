import 'package:flutter/material.dart';
import '../../models/wings.dart';
import './item.dart';


Widget wingShowCase() {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 30),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: wingList.wings.length,
      itemBuilder: (BuildContext context, int i) {
        return ListOfWings(
          foreground: wingList.wings[i].foreground,
          background: wingList.wings[i].background,
          price: wingList.wings[i].price,
          name: wingList.wings[i].name,
          image: wingList.wings[i].image,
          wingObject: wingList.wings[i],
        );
      },
    ),
  );
}