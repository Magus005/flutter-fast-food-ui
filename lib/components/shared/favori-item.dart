import 'package:flutter/material.dart';


class StatefulFavIcon extends StatefulWidget {
  const StatefulFavIcon({@required this.foreground});

  final Color foreground;

  @override
  StatefulFavIconState createState() => StatefulFavIconState();
}

class StatefulFavIconState extends State<StatefulFavIcon> {
  bool isFavori;

  @override
  void initState() {
    super.initState();
    isFavori = false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isFavori = !isFavori;
        });
      },
      child: Icon(
        isFavori ? Icons.favorite : Icons.favorite_border,
        color: widget.foreground
      ),
    );
  }

}
