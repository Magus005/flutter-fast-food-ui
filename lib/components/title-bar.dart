import 'package:flutter/material.dart';


Widget titleBar() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 70),
          Text("Fast", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 50)),
          Text("Food", style: TextStyle(fontSize: 50))
        ],
      )
    ],
  );
}