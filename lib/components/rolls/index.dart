import 'package:flutter/material.dart';
import '../../models/rolls.dart';
import './item.dart';


Widget rollShowCase() {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 30),
    child: ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: rollList.rolls.length,
      itemBuilder: (BuildContext context, int i) {
        return ListOfRolls(
          foreground: rollList.rolls[i].foreground,
          background: rollList.rolls[i].background,
          price: rollList.rolls[i].price,
          name: rollList.rolls[i].name,
          image: rollList.rolls[i].image,
          rollObject: rollList.rolls[i],
        );
      },
    ),
  );
}