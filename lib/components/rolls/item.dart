import 'package:flutter/material.dart';
import '../../models/rolls.dart';
import '../shared/favori-item.dart';
import '../../details-roll.dart';


class ListOfRolls extends StatelessWidget {

  const ListOfRolls({
    @required this.foreground,
    @required this.background,
    @required this.price,
    @required this.name,
    @required this.image,
    @required this.rollObject,
  });

  final Color foreground;
  final Color background;
  final double price;
  final String name;
  final String image;
  final Roll rollObject;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => DetailsRoll(rollObject)
            ));
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 35, horizontal: 20),
            width: 250,
            decoration: BoxDecoration(
              color: background,
              borderRadius: BorderRadius.circular(40)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 180,
                  child: Image.asset(image),
                ),

                RichText(
                  softWrap: true,
                  text: TextSpan(
                    style: TextStyle(color: foreground, fontSize: 25, fontFamily: "slabo"),
                    children: [
                      TextSpan(text: name),
                      TextSpan(text: "\nRoll", style: TextStyle(fontWeight: FontWeight.w800))
                    ]
                  ),
                ),
                
                SizedBox(height: 40),

                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text("${price} XOF", style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20, color: foreground, fontFamily: "arial")),
                    ),
                    
                    StatefulFavIcon(foreground: foreground)
                  ]
                ),
              ],
            ),
          ),
        ),
        

        SizedBox(width: 40)


      ],
    );
  }

}